import 'package:flutter/material.dart';

Row _bildTextTitle(String lable, String des) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Text(lable,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: 'Palette',
                color: Colors.indigo[900])),
        Text(des,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18,
                fontFamily: 'THSarabun')),
      ]);
}

Row _bildTextFont(String lable) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Text(lable,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
              fontFamily: 'Bangna',
            )),
      ]);
}

Column _bildImage(String img) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Image.asset(
        img,
        width: 50,
        height: 50,
        fit: BoxFit.contain,
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('RESUME',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          fontFamily: 'Vogue')),
                ),
              ])),
        ],
      ),
    );
    Widget name = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _bildTextTitle('ชื่อ: ', 'พิสิฐพงศ์ '),
          _bildTextTitle('นามสกุล: ', 'นาถธรรมกุล'),
        ],
      ),
    );
    Widget name2 = Container(
      // margin: const EdgeInsets.only(left: 150,right: 150),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _bildTextTitle('ชื่อเล่น: ', 'เต๋า'),
              _bildTextTitle('เพศ: ', 'ชาย'),
              _bildTextTitle('อายุ: ', '22ปี'),
            ],
          )),
        ],
      ),
    );
    Widget headcompSkill = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/coding.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' Computer Skill',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );
    Widget compSkill = Container(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              _bildImage('images/c.png'),
              _bildImage('images/vue.png'),
              _bildImage('images/java.png'),
              _bildImage('images/html.png'),
              _bildImage('images/python.png'),
            ],
          )),
        ],
      ),
    );
    Widget titleTools = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/coding.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' Tools ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );
    Widget tool1 = Container(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              _bildImage('images/js.png'),
              _bildImage('images/git.png'),
            ],
          )),
        ],
      ),
    );
    Widget titlePortfolio = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/resume.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' ประวัติส่วนตัว',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );
    Widget titleEducation = Container(
      padding: const EdgeInsets.only(top: 25, left: 25, bottom: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/graduation-hat.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' การศึกษา',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );

    return MaterialApp(
        title: 'RESUME',
        theme: new ThemeData(
            scaffoldBackgroundColor: const Color.fromRGBO(100, 90, 134, 0.6)),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.pink[50],
          ),
          body: ListView(
            children: [
              titleSection,
              Image.asset(
                'images/profile.jpg',
                width: 240,
                height: 272,
                fit: BoxFit.contain,
              ),
              name,
              name2,
              headcompSkill,
              compSkill,
              titleTools,
              tool1,
              titlePortfolio,
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('วันเกิด: ', '13 , มกราคม 2000'),
                  ],
                ))
              ])),
              Container(
                  // padding: const EdgeInsets.only(left: 50),
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('เชื้อชาติ: ', 'ไทย'),
                    _bildTextTitle('สัญชาติ: ', 'ไทย'),
                    _bildTextTitle('ศาสนา: ', 'พุทธ'),
                  ],
                ))
              ])),
              Container(
                  // padding: const EdgeInsets.only(left: 50),
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('ที่อยู่: ', '369'),
                    _bildTextTitle('อำเภอ: ', 'เมือง'),
                    _bildTextTitle('จังหวัด: ', 'ชลบุรี'),
                  ],
                ))
              ])),
              Container(
                  // padding: const EdgeInsets.only(left: 50),
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('ถนน: ', 'วชิรปราการ'),
                    _bildTextTitle('ตำบล: ', 'บางปลาสร้อย'),
                    _bildTextTitle('ไปรษณี: ', '20000'),
                  ],
                ))
              ])),
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('อีเมล: ', 'pisitpong13@hotmail.com'),
                  ],
                ))
              ])),
              titleEducation,
              Container(
                padding: const EdgeInsets.only(left: 50, bottom: 5),
                margin: EdgeInsets.all(10),
                child: Table(
                  children: [
                    TableRow(children: [
                      Column(children: [
                        _bildTextFont("ระดับการศึกษา"),
                        _bildTextFont('อนุบาล'),
                        _bildTextFont('ประถม'),
                        _bildTextFont('มัธยมต้น'),
                        _bildTextFont('มัธยมปลาย'),
                        _bildTextFont('อุดมศึกษา'),
                      ]),
                      Column(children: [
                        _bildTextFont('รายละเอียด'),
                        _bildTextFont('โรงเรียนอนุบาลชลบุรี'),
                        _bildTextFont('โรงเรียนอนุบาลชลบุรี'),
                        _bildTextFont('โรงเรียนชลราษฎรอำรุง'),
                        _bildTextFont('โรงเรียนชลราษฎรอำรุง'),
                        _bildTextFont('มหาวิทยาลัยบูรพา'),
                      ]),
                    ]),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
